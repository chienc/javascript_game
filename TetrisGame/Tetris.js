let canvas = document.getElementById('tetris');
let ctx = canvas.getContext('2d');

const row = 20;
const column = 10;
const squareSize = 30;
const emptySquareColour = 'white';

let gameover = false;
let score = 0;

// draw the square
function drawSquare(x, y, colour) {
    ctx.fillStyle = colour;
    ctx.fillRect(x*squareSize, y*squareSize, squareSize, squareSize);
    ctx.strokeStyle = 'black';
    ctx.strokeRect(x*squareSize, y*squareSize, squareSize, squareSize);
}

// create the board
let board = [];

for(r = 0; r < row; r++) {
    board[r] = [];
    for(c = 0; c < column; c++) {
        board[r][c] = emptySquareColour;
    }
}

function drawBoard() {
    for(r = 0; r < row; r++) {
        for(c = 0; c < column; c++) {
            drawSquare(c, r, board[r][c]);
        }
    }
}

drawBoard();

//form and colour of pieces
const Z = [
    [
        [1, 1, 0],
        [0, 1, 1],
        [0, 0, 0]
    ],
    [
        [0, 0, 1],
        [0, 1, 1],
        [0, 1, 0]
    ],
    [
        [0, 0, 0],
        [1, 1, 0],
        [0, 1, 1]
    ],
    [
        [0, 1, 0],
        [1, 1, 0],
        [1, 0, 0]
    ]
];
const S = [
    [
        [0, 1, 1],
        [1, 1, 0],
        [0, 0, 0]
    ],
    [
        [0, 1, 0],
        [0, 1, 1],
        [0, 0, 1]
    ],
    [
        [0, 0, 0],
        [0, 1, 1],
        [1, 1, 0]
    ],
    [
        [1, 0, 0],
        [1, 1, 0],
        [0, 1, 0]
    ]
];
const J = [
    [
        [1, 0, 0],
        [1, 1, 1],
        [0, 0, 0]
    ],
    [
        [0, 1, 1],
        [0, 1, 0],
        [0, 1, 0]
    ],
    [
        [0, 0, 0],
        [1, 1, 1],
        [0, 0, 1]
    ],
    [
        [0, 1, 0],
        [0, 1, 0],
        [1, 1, 0]
    ]
];
const T = [
    [
        [0, 1, 0],
        [1, 1, 1],
        [0, 0, 0]
    ],
    [
        [0, 1, 0],
        [0, 1, 1],
        [0, 1, 0]
    ],
    [
        [0, 0, 0],
        [1, 1, 1],
        [0, 1, 0]
    ],
    [
        [0, 1, 0],
        [1, 1, 0],
        [0, 1, 0]
    ]
];
const L = [
    [
        [0, 0, 1],
        [1, 1, 1],
        [0, 0, 0]
    ],
    [
        [0, 1, 0],
        [0, 1, 0],
        [0, 1, 1]
    ],
    [
        [0, 0, 0],
        [1, 1, 1],
        [1, 0, 0]
    ],
    [
        [1, 1, 0],
        [0, 1, 0],
        [0, 1, 0]
    ]
];
const I = [
    [
        [0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
    ],
    [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
    ]
];
const O = [[[0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 0],]];

const piecesColour = [
    [Z, '#ff4c4c'],
    [S, '#009f4d'],
    [T, '#ffc845'],
    [O, '#b84592'],
    [L, '#f48924'],
    [I, '#84bd00'],
    [J, '#00bce4']
]

// the object Piece
function Piece(tetromino, colour) {
    this.tetromino = tetromino;
    this.colour = colour;

    this.tetrominoNum = 0;
    this.activateTetromino = this.tetromino[this.tetrominoNum];

    this.x = 3;
    this.y = -2;
}

//initiate a random piece from piecesColour
function randomPiece() {
    let num = Math.floor(Math.random() * piecesColour.length);
    return new Piece(piecesColour[num][0], piecesColour[num][1])
}

let p = randomPiece();

// fill the board
Piece.prototype.fill = function(colour) {
    for(r = 0; r < this.activateTetromino.length; r++) {
        for(c = 0; c < this.activateTetromino.length; c++) {
            if(this.activateTetromino[r][c]) {
                drawSquare(this.x + c, this.y + r, colour);
            }
        }
    }
}

// draw the piece
Piece.prototype.draw = function() {
    this.fill(this.colour);
}

// undraw the piece
Piece.prototype.undraw = function() {
    this.fill(emptySquareColour);
}

// movement of the piece
Piece.prototype.moveDown = function() {
    if(!this.collision(0, 1, this.activateTetromino)) {
        this.undraw();
        this.y++;
        this.draw();
    } else {
        this.fix();
        p = randomPiece();
    }
}

Piece.prototype.moveLeft = function() {
    if(!this.collision(-1, 0, this.activateTetromino)) {
        this.undraw();
        this.x--;
        this.draw();
    }
}

Piece.prototype.moveRight = function() {
    if(!this.collision(1, 0, this.activateTetromino)) {
        this.undraw();
        this.x++;
        this.draw();
    }
}

Piece.prototype.rotate = function() {
    let nextPattern = this.tetromino[(this.tetrominoNum + 1) % this.tetromino.length]
    let xAdjustment = 0;
    // make adjustment if the next pattern hit the wall or existing piece
    if(this.collision(0, 0, nextPattern)) {
        if(this.x > column/2) {
            xAdjustment = -1;
        } else {
            xAdjustment = 1;
        }
    }
    // check collision with xAdjustment
    if(!this.collision(xAdjustment, 0, nextPattern)) {
        this.undraw();
        this.tetrominoNum = (this.tetrominoNum + 1) % this.tetromino.length;
        this.activateTetromino = this.tetromino[this.tetrominoNum];
        this.draw();
    }
}

// check collision
Piece.prototype.collision = function(x, y, piece) {
    for(r = 0; r < piece.length; r++) {
        for(c = 0; c < piece.length; c++) {
            // skip the empty square
            if(!piece[r][c]) {
                continue;
            }
            // new coordinates after movement
            let newX = this.x + c + x;
            let newY = this.y + r + y;
            // out of boarder
            if(newX < 0 || newX >= column || newY >= row) {
                return true
            }
            // skip newY < 0
            if(newY < 0) {
                continue;
            }
            // the space is taken by another piece
            if(board[newY][newX] != emptySquareColour) {
                return true;
            }
        }
    }
    return false
}

// fix the piece
Piece.prototype.fix = function() {
    for(r = 0; r < this.activateTetromino.length; r++) {
        for(c = 0; c < this.activateTetromino.length; c++) {
            if(!this.activateTetromino[r][c]) {
                continue;
            } 
            // pieces have reached the top
            if(this.y + r < 0) {
                alert('GAME OVER!');
                gameover = true;
                break;
            }
            // fix the piece
            board[this.y + r][this.x + c] = this.colour;
        }
    }
    // remove full row
    for(r = 0; r < row; r++) {
        let fullRow = true;
        for(c = 0; c < column; c++) {
            fullRow = fullRow && (board[r][c] != emptySquareColour);
        }
        if(fullRow) {
            for(y = r; y > 1; y--) {
                for(c = 0; c < column; c++) {
                    board[y][c] = board[y-1][c];
                }
            }
            // recreate the top row
            for(c = 0; c < column; c++) {
                board[0][c] = emptySquareColour;
            }
            // update the score
            score += 10;
            document.getElementById('score').textContent = score;
        }    
    }
    // update the board
    drawBoard();
}

// keyboard control
document.addEventListener('keydown', control);

function control(e) {
    switch(e.keyCode) {
        case 37:
            p.moveLeft();
            dropStart = Date.now();
            break;
        case 39:
            p.moveRight();
            dropStart = Date.now();
            break;
        case 40:
            p.moveDown();
            dropStart = Date.now();
            break;
        case 32:
            p.rotate();
            dropStart = Date.now();
            break;
    }
}


// piece falling down
let dropStart = Date.now();
function drop() {
    let now = Date.now();
    let delta = now - dropStart;
    if(delta > 1000) {
        p.moveDown();
        dropStart = Date.now();
    }
    if(!gameover) {
        requestAnimationFrame(drop);
    }
}

p.draw();
drop();

